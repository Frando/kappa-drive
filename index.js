const Kappa = require('kappa-core')
const Indexer = require('kappa-sparse-indexer')
const Nanoresource = require('nanoresource/emitter')
const KV = require('kappa-view-kv')
const multifeed = require('multifeed')
const debug = require('debug')
const memdb = require('memdb')
const corestore = require('corestore')
const MountableHypertrie = require('mountable-hypertrie')
const hyperdrive = require('hyperdrive')
const duplexify = require('duplexify')
const ram = require('random-access-memory')
const through = require('through2')
const collect = require('collect-stream')
const unique = require('unique-stream')
const containsPath = require('contains-path')
const crypto = require('hypercore-crypto')
const fs = require('fs')
const protobuf = require('protocol-buffers')
const path = require('path')
const errors = require('hyperdrive/lib/errors')
const maybe = require('call-me-maybe')
const assert = require('assert')
const sub = require('subleveldown')
const { Header, Node } = require('hypertrie/lib/messages')
const { Stat } = require('hyperdrive-schemas')

const { normalize, logCpuMemory, uniq } = require('./util')
const { dumbMerge, vectorClock } = require('./merge')
const { State } = require('./messages')

const METADATA = 'metadata'
const CONTENT = 'content'

const BAD_FILE_DESCRIPTOR = 'kappa-drive: bad file descriptor'

const WRITEFLAGS = ['w']
const READFLAGS = ['r']
const RANDOMFLAGS = ['a', 'ax', 'as', 'wx', 'a+', 'ax+', 'r+', 'rs+', 'w+', 'wx+']

class KappaDrive extends Nanoresource {
  constructor (storage, key, opts) {
    if (!Buffer.isBuffer(key) && !opts) {
      opts = key
      key = null
    }
    if (!opts) opts = {}

    super()

    this._id = opts._id || crypto.randomBytes(2).toString('hex')

    this.key = key
    this.discoveryKey = null
    this.keyPair = opts.keyPair || noop

    this._storage = storage
    this._db = opts.db || memdb()
    this._resolveFork = opts.resolveFork || vectorClock || numberLinks
    this._feedIds = opts.feedIds || [1, 2]
    this._fileDescriptors = {}
    this._drives = {}
    this._opts = opts

    this.core = opts.core || new Kappa()

    this._feeds = opts.multifeed || multifeed(storage, Object.assign({
      encryptionKey: key
    }, opts))

    this._indexer = opts.indexer || new Indexer({
      db: sub(this._db, 'idx'),
      name: this._id
    })

    this.vectorClock = {}
    this.log = opts.logger || debug('kappa-drive')

    const onfeed = (feed) => {
      if (this._indexer.feed(feed.key)) return
      scopeFeeds(feed, (err) => {
        if (!err) this._indexer.add(feed, { scan: true })
      })
    }

    this._feeds.feeds().forEach(onfeed)
    this._feeds.on('feed', onfeed)
    this._feeds.on('download', onfeed)
  }

  ready (callback) {
    return this.open((err) => {
      if (err) return callback(err)
      this.core.ready('drive', callback)
    })
  }

  replicate (isInitiator, opts = {}) {
    return this._feeds.replicate(isInitiator, Object.assign({}, {
      live: true,
      maxFeeds: 1024
    }, opts))
  }

  // open and close inherited from nanoresource to ensure process is only made active
  // once clash with fs operations. super called to pass down the stack, otherwise
  // fs operations are handled normally

  open (filename, flags, callback) {
    if (!filename || typeof filename === 'function') return super.open(filename)

    var handler = this._getFlagHandler(flags)
    if (!handler) return callback(new Error(`kappa-drive: missing flag handler for flag ${flags}`))
    handler(filename, flags, callback)
  }

  close (fd, callback) {
    if (typeof fd === 'number') return this._closeFile(fd, callback)
    super.close(false, fd)
  }

  _closeFile(fd, callback) {
    const { filename, writeMode } = this._fileDescriptors[fd]
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))
    if (writeMode) {
      this._getLinks(filename, (err, links) => {
        if (err) return callback(err)
        this.drive.close(fd, (err) => {
          if (err) return callback(err)
          delete this._fileDescriptors[fd]
          this._finishWrite(filename, links, callback)
        })
      })
    } else {
      this._drives[this._fileDescriptors[fd].driveKey.toString('hex')].close(fd, (err) => {
        if (err) return callback(err)
        delete this._fileDescriptors[fd]
        return callback()
      })
    }
  }

  // -------------- READ ACTIONS ----------------

  read (fd, buf, offset, len, position, callback) {
    const { filename, writeMode } = this._fileDescriptors[fd]
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))
    var drive = writeMode
      ? this.drive
      : this._drives[this._fileDescriptors[fd].driveKey.toString('hex')]

    // TODO: this is a hack - the file should already be open
    drive.open(filename, 'r', (err, newFd) => {
      if (err) return callback(err)
      drive.read(newFd, buf, offset, len, position, callback)
    })
  }

  readFile (filename, opts, callback) {
    if (typeof opts === 'function') return this.readFile(filename, null, opts)
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.readFile(filename, opts, callback)
    })
  }

  createReadStream (filename) {
    var proxy = duplexify()
    this._whoHasFile(filename, (err, drive) => {
      if (err) return proxy.emit('err', err)
      proxy.setReadable(drive.createReadStream(filename))
    })
    return proxy
  }

  exists (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.exists(filename, opts, callback)
    })
  }

  readdir (name, opts, callback) {
    if (typeof opts === 'function') return this.readdir(name, null, opts)

    name = normalize(name)

    var self = this

    this.core.ready('drive', () => {
      var stream = this.core.view.drive.createReadStream()
      var t = stream.pipe(through.obj(function (chunk, enc, next) {
        if (!containsPath(chunk.key, `./${name}`)) return next()

        self.exists(chunk.key, opts, (exists) => {
          if (exists) {
            const filePath = normalize(chunk.key.substring(name.length))
            const filename = filePath.split('/')[0]
            if (filename !== '') this.push({ filename })
          }
          next()
        })
      }))

      collect(t.pipe(unique()), (err, data) => {
        if (err) return callback(err)
        var files = data.map(d => d.filename)
        callback(null, files)
      })
    })
  }

  stat (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.stat(filename, opts, callback)
    })
  }

  lstat (filename, opts, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.lstat(filename, opts, callback)
    })
  }

  // --------------- WRITE ACTIONS -----------------

  write (fd, buf, offset, len, pos, callback) {
    if (typeof pos === 'function' && !callback) return this.write(fd, buf, offset, len, null, pos)

    const { filename } = this._fileDescriptors[fd]
    if (!filename) return callback(new errors.BadFileDescriptor(BAD_FILE_DESCRIPTOR))
    this.drive.write(fd, buf, offset, len, pos, callback)
  }

  writeFile (filename, data, callback) {
    this._getLinks(filename, (err, links) => {
      if (err) return callback(err)
      this.drive.writeFile(filename, data, (err) => {
        if (err) return callback(err)
        this._finishWrite(filename, links, callback)
      })
    })
  }

  createWriteStream (filename) {
    var proxy = duplexify()
    this._getLinks(filename, (err, links) => {
      if (err) proxy.emit('error', err)
      var writer = this.drive.createWriteStream(normalize(filename))
      proxy.setWritable(writer)

      var prefinish = () => {
        proxy.cork()
        this._finishWrite(filename, links, (err) => {
          if (err) return proxy.destroy()
          proxy.uncork()
        })
      }

      proxy.on('close', done)
      proxy.on('finish', done)
      proxy.on('prefinish', prefinish)

      function done () {
        proxy.removeListener('close', done)
        proxy.removeListener('finish', done)
        proxy.removeListener('prefinish', prefinish)
      }
    })

    return proxy
  }

  symlink (target, dest, callback) {
    this._getLinks(target, (err, links) => {
      if (err) return callback(err)
      this.drive.symlink(target, dest, (err) => {
        if (err) return callback(err)
        this._finishWrite(target, links, callback)
      })
    })
  }

  unlink (target, callback) {
    this._getLinks(target, (err, links) => {
      if (err) return callback(err)
      if (!links.length) return callback(new errors.FileNotFound(target))
      // Do a write action, otherwise unlink will throw an error
      this.drive.writeFile(target, '', (err) => {
        if (err) return callback(err)
        this.drive.unlink(target, callback)
      })
    })
  }

  truncate (filename, size, callback) {
    var self = this
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      if (drive.key === this.drive.key) return performTruncate()
      drive.open(filename, 'r', (err, fd) => {
        if (err) return callback(err)
        var data = Buffer.alloc(size)
        drive.read(fd, data, 0, size, null, (err) => {
          if (err) return callback(err)
          drive.close(fd, (err) => {
            if (err) return callback(err)
            this._getLinks(filename, (err, links) => {
              if (err) return callback(err)
              this.drive.writeFile(filename, data, (err) => {
                if (err) return callback(err)
                this._finishWrite(filename, links, callback)
              })
            })
          })
        })
      })

      function performTruncate () {
        self._getLinks(filename, (err, links) => {
          if (err) return callback(err)
          self.drive.truncate(filename, size, (err) => {
            if (err) return callback(err)
            self._finishWrite(filename, links, callback)
          })
        })
      }
    })
  }

  rename (source, destination, callback) {
    this.readFile(source, (err, data) => {
      if (err) return callback(err)
      this.writeFile(destination, data, (err) => {
        if (err) return callback(err)
        this.unlink(source, (err) => {
          if (err) return callback(err)
          callback()
        })
      })
    })
  }

  // TODO: maybe this depends on previous state (should throw err if already exists)
  mkdir (name, mode, callback) {
    // NOTE: hyperdrive's mkdir doesnt take mode, fs.mkdir does
    if (typeof mode === 'function') return this.mkdir(name, null, mode)
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive.mkdir(name, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  rmdir (name, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      if (!links.length) return callback(new errors.FileNotFound(name))
      // See if we have it, otherwise rmdir throws an error
      this.drive.stat(name, (err, statObj) => {
        if (err) return this._finishWrite(name, links, callback)
        this.drive.rmdir(name, callback)
      })
    })
  }

  create (name, opts, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive.create(name, opts, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  // -------------------- PRIVATE FUNCTIONS -----------------------

  _whoHasFile (filename, callback) {
    filename = normalize(filename)

    this.log(logCpuMemory(this._id))

    this.core.ready('drive', () => {
      this.core.view.drive.get(filename, (err, msgs = []) => {
        if (err && !err.notFound) return callback(err)
        var values = msgs.map((msg) => tryDecode(msg.value))
        if (!values || !values.length) return callback(null, this.drive)
        var winner = this._resolveFork(values, this.vectorClock)
        var metadata = this._feeds.feed(winner.metadata.id)
        if (!metadata) return callback(new Error('kappa-drive: invalid key for metadata'))
        var content = this._feeds.feed(winner.content.id)
        if (!content) return callback(new Error('kappa-drive: invalid key for content'))
        this._getDrive(metadata, content, callback)
      })
    })
  }

  // this is used for chmod and chown
  _update (name, opts, callback) {
    this._getLinks(name, (err, links) => {
      if (err) return callback(err)
      this.drive._update(name, opts, (err) => {
        if (err) return callback(err)
        this._finishWrite(name, links, callback)
      })
    })
  }

  _open (callback) {
    this._feeds.ready((err) => {
      if (err) return callback(err)
      this.key = this._feeds._root.key

      const opts = this._feedIds.map((n) => ({
        keypair: this.keyPair(n, this.key)
      }))

      this._feeds.writer(METADATA, opts.shift(), (err, metadata) => {
        if (err) return callback(err)
        this.metadata = metadata

        this._feeds.writer(CONTENT, opts.shift(), (err, content) => {
          if (err) return callback(err)
          this.content = content

          this._getDrive(this.metadata, this.content, (err, drive) => {
            this.discoveryKey = this._feeds._root.discoveryKey
            this.drive = drive

            var view = KV(sub(this._db, 'view'), this._createMap(), {
              getFeed: (key) => this._feeds.feed(key)
            })

            this.core.use('drive', this._indexer.source(), view)

            this.core.view.drive.onUpdate((key, entry) => {
              this.emit('update', key, entry)
              this.drive.key.toString('hex') === entry.key
                ? this.emit('local-update', key, entry)
                : this.emit('remote-update', key, entry)
            })

            return callback()
          })
        })
      })
    })
  }

  _close (callback = noop) {
    this.core.close((err) => {
      if (err) return callback(err)
      this._feeds.close(callback)
    })
  }

  _getDrive (metadata, content, opts = {}, callback) {
    if (typeof opts === 'function' && !callback) return this._getDrive(metadata, content, {}, opts)

    if (this._drives[metadata.key.toString('hex')]) {
      var drive = this._drives[metadata.key.toString('hex')]
      return drive.ready((err) => callback(err, drive))
    }

    var store = corestore(this.storage, { defaultCore: metadata })

    var drive = hyperdrive(ram, metadata.key, {
      metadata,
      _content: content,
      _db: new MountableHypertrie(store, metadata.key, {
        feed: metadata,
        sparse: this.sparseMetadata
      })
    })

    // TODO: drive = opts.version ? drive.checkout(opts.version) : drive
    this._drives[metadata.key.toString('hex')] = drive

    drive.ready((err) => callback(err, drive))
  }

  _getLinks (filename, callback) {
    filename = normalize(filename)
    this.core.ready('drive', () => {
      this.core.view.drive.get(filename, (err, msgs) => {
        if (err && !err.notFound) return callback(err)
        var links = msgs ? msgs.map((msg) => [msg.key, msg.seq].join('@')) : []
        return callback(null, links)
      })
    })
  }

  // TODO: store drive version
  _finishWrite (filename, links, callback) {
    this.vectorClock[this.metadata.key.toString('hex')] += 1

    var state = {
      filename: normalize(filename),
      version: this.drive.version,
      links,
      metadata: {
        id: this.metadata.key.toString('hex'),
        seq: this.metadata.length
      },
      content: {
        id: this.content.key.toString('hex'),
        seq: this.content.length
      },
      timestamp: Date.now(),
      vectorClock: this.vectorClock
    }

    var payload = State.encode(state)

    const opts = {
      metadata: {
        peerfs: payload
      }
    }

    this.drive._update(filename, opts, callback)
  }

  // ---------------- Read / Write / Random Access Flag Handlers -----------------

  _getFlagHandler (flag) {
    if (typeof flag === 'number') {
      const mode = flag & 3
      if (mode === 0) return this._handleRead.bind(this)
      return this._handleRandomAccess.bind(this)
    }

    var api = {
      [READFLAGS]: this._handleRead.bind(this),
      [WRITEFLAGS]: this._handleWrite.bind(this),
      [RANDOMFLAGS]: this._handleRandomAccess.bind(this)
    }

    var action = Object.keys(api)
      .map((flags) => flags.split(','))
      .find((flags) => flags.includes(flag.toString()))

    var handler = api[action]
    if (!handler) throw new Error(`kappa-drive: missing handler for ${flag}`)

    return handler
  }

  _handleWrite (filename, flags, callback) {
    this.drive.open(filename, flags, (err, fd) => {
      if (err) return callback(err)
      this._fileDescriptors[fd] = { filename, writeMode: true }
      return callback(null, fd)
    })
  }

  _handleRead (filename, flags, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err) return callback(err)
      drive.open(filename, flags, (err, fd) => {
        if (err) return callback(err)
        this._fileDescriptors[fd] = { filename, writeMode: false, driveKey: drive.metadata.key.toString('hex') }
        return callback(null, fd)
      })
    })
  }

  _handleRandomAccess (filename, flags, callback) {
    this._whoHasFile(filename, (err, drive) => {
      if (err || drive.metadata.key === this.drive.metadata.key) return this._handleWrite(filename, flags, callback)
      drive.readFile(filename, (err, data) => {
        if (err) return this._handleWrite(filename, flags, callback)
        this.writeFile(filename, data, (err) => {
          if (err) return callback(err)
          this._handleWrite(filename, flags, callback)
        })
      })
    })
  }

  _createMap () {
    var self = this
    var ownKey = this.metadata.key.toString('hex')
    this.vectorClock[ownKey] = this.vectorClock[ownKey] || 0

    return function (msg, next) {
      var ops = []

      const state = tryDecode(msg.value)
      if (!state) return next()

      if (state.metadata.id !== ownKey) {
        self.vectorClock[ownKey] += 1
        self.vectorClock = combineVectors(self.vectorClock, state.vectorClock)
      }

      ops.push({
        key: state.filename,
        id: [msg.key, msg.seq].join('@'),
        links: (state.links || []),
        metadata: {
          id: state.metadata.id.toString('hex'),
          seq: state.metadata.seq
        },
        content: {
          id: state.content.id.toString('hex'),
          seq: state.content.seq
        },
        vectorClock: state.vectorClock
      })

      next(null, ops)

      function combineVectors (a, b) {
        var result = {}
        Object.keys(a).forEach(key => {
          result[key] = Math.max(a[key], b[key] || 0)
        })
        Object.keys(b).forEach(key => {
          result[key] = result[key] || b[key]
        })
        return result
      }
    }
  }
}


function scopeFeeds (feed, next) {
  feed.get(0, (err, msg) => {
    if (err) return next()

    try {
      var header = Header.decode(msg)
      if (header.type !== 'hypertrie') return next(new Error('invalid feed'))
      next()
    } catch (err) {
      return next(err)
    }
  })
}

function noop () {}

function tryDecode (value) {
  let stat = decode(Stat, value)
  if (!stat) {
    const node = decode(Node, value)
    if (node && node.valueBuffer) {
      stat = decode(Stat, node.valueBuffer)
    }
  }

  let state = null
  if (stat && stat.metadata && stat.metadata.peerfs) {
    state = decode(State, stat.metadata.peerfs)
  }

  return state

  function decode (encoding, value) {
    try {
      return encoding.decode(value)
    } catch (err) { return null }
  }
}

module.exports = (storage, key, opts) => new KappaDrive(storage, key, opts)
module.exports.KappaDrive = KappaDrive
module.exports.tryDecode = tryDecode
module.exports.scopeFeeds = scopeFeeds
