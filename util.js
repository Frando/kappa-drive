const unixify = require('unixify')
const path = require('path')

function virtualize (p) {
  return unixify(path.resolve('/', p))
}

function normalize (p) {
  return unixify(path.resolve('/', p))
    .replace(/^\//g, '')
}

function logCpuMemory (id) {
  return {
    id,
    cpuUsage: process.cpuUsage(),
    memoryUsage: process.memoryUsage()
  }
}

function uniq (array) {
  if (!Array.isArray(array)) array = [array]
  return Array.from(new Set(array))
}

module.exports = { virtualize, normalize, logCpuMemory, uniq }
