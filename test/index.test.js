const { describe } = require('tape-plus')
const ram = require('random-access-memory')
const crypto = require('hypercore-crypto')
const fs = require('fs')
const sodium = require('sodium-native')
const hypercore = require('hypercore')

const KappaDrive = require('../')

const { replicate, tmp, cleanup, uniq } = require('./util')

describe('basic', (context) => {
  context('write and read latest value', (assert, next) => {
    var drive = KappaDrive(ram)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.readFile('/hello.txt', (err, content) => {
          assert.error(err, 'no error')
          assert.same(content, Buffer.from('world'))
          next()
        })
      })
    })
  })

  context('writeStream and readStream', (assert, next) => {
    var drive = KappaDrive(ram)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      var ws = drive.createWriteStream('/hello.txt')
      ws.end('world')
      ws.on('finish', () => {
        var rs = drive.createReadStream('/hello.txt')
        rs.on('data', (data) => {
          assert.same(data, Buffer.from('world'))
          next()
        })
      })
    })
  })

  context('open', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/hello.txt', 'w+', function (err, fd) {
        assert.error(err, 'no error')
        assert.same(typeof fd, 'number', "returns a reference to the drive's file descriptor")
        cleanup(storage, next)
      })
    })
  })

  context('close', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/hello.txt', 'w+', function (err, fd) {
        assert.error(err, 'no error')
        drive.close(fd, function (err) {
          assert.error(err, 'closes without error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('write', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.open('/bonjour.txt', 'w', function (err, fd) {
        assert.error(err, 'no error')
        var data = Buffer.from('monde')
        drive.write(fd, data, 0, 5, null, (err, bytesWritten, writeData) => {
          assert.error(err, 'drive.write returns no error')
          assert.same(bytesWritten, 'monde'.length, 'correct number of bytes written')
          // i think hyperdrive does not return the buffer in the cb, unlike fs.write
          // assert.same(writeData, Buffer.from('monde'))
          drive.close(fd, function (err) {
            assert.error(err, 'no error')
            drive.readFile('./bonjour.txt', (err, contents) => {
              assert.error(err, 'no error')
              assert.same(contents, Buffer.from('monde'), 'file successfully written')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('read', (assert, next) => {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/bonjour.txt', 'monde', (err) => {
        assert.error(err, 'no error')
        drive.open('/bonjour.txt', 'r', function (err, fd) {
          assert.error(err, 'no error')
          var data = Buffer.alloc('monde'.length)
          drive.read(fd, data, 0, 5, null, (err, bytesRead, readData) => {
            assert.error(err, 'drive.read returns no error')
            assert.same(readData, Buffer.from('monde'))
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('exists', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.exists('/hello.txt', function (bool) {
        assert.notOk(bool, 'no file yet')
        drive.writeFile('/hello.txt', 'world', function (err) {
          assert.error(err, 'no error')
          drive.exists('/hello.txt', function (bool) {
            assert.ok(bool, 'found file')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('stat', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.stat('/hello.txt', (err, stats) => {
          assert.error(err, 'no error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('lstat', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.lstat('/hello.txt', (err, stats) => {
          assert.error(err, 'no error')
          cleanup(storage, next)
        })
      })
    })
  })

  context('symlink', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.symlink('/hello.txt', '/world.txt', (err) => {
          assert.error(err, 'no error')
          drive.readFile('/world.txt', (err, data) => {
            assert.error(err, 'no error')
            assert.same(data, Buffer.from('world'), 'symlinked')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('truncate', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.truncate('/hello.txt', 1, (err) => {
          assert.error(err, 'no error')
          drive.readFile('/hello.txt', (err, data) => {
            assert.error(err, 'no error')
            assert.deepEqual(Buffer.from('w'), data, 'truncated file')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('unlink', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.unlink('/hello.txt', (err) => {
          assert.error(err, 'no error')
          drive.readFile('/hello.txt', (err, data) => {
            assert.ok(err, 'file no longer exists')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  // TODO: symlink

  context('_update', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        // chmod a+x
        drive._update('/hello.txt', { mode: fs.constants.S_IXOTH }, (err) => {
          assert.error(err, 'no error')
          drive.stat('/hello.txt', (err, stat) => {
            assert.error(err, 'no error')
            assert.same(stat.mode, fs.constants.S_IXOTH)
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('mkdir and rmdir', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.mkdir('/hello', (err) => {
        assert.error(err, 'no error')
        drive.readdir('/', (err, files) => {
          assert.error(err, 'no error')
          assert.same(files, ['hello'], 'created directory is listed')
          drive.rmdir('/hello', (err) => {
            assert.error(err, 'no error')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('rename', (assert, next) => {
    var drive = KappaDrive(ram)

    drive.ready(() => {
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.rename('/hello.txt', '/goodbye.txt', (err) => {
          assert.error(err, 'no error')
          drive.stat('/hello.txt', (err) => {
            assert.ok(err, 'statting source fail no longer possible')
            drive.readFile('/goodbye.txt', 'utf-8', (err, data) => {
              assert.error(err)
              assert.same(data, 'world', 'destination has correct content')
              next()
            })
          })
        })
      })
    })
  })

  context('keys', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    drive.ready((err) => {
      assert.error(err, 'no error')
      assert.ok(Buffer.isBuffer(drive.key), 'drive.key returns a buffer')
      assert.ok(Buffer.isBuffer(drive.discoveryKey), 'drive.discoveryKey returns a buffer')
      cleanup(storage, next)
    })
  })

  context('readdir', function (assert, next) {
    var storage = tmp()
    var drive = KappaDrive(storage)

    const filesToWrite = ['stuff/things/ape.txt', 'badger_number_one.txt']
    const expectedReaddirOutput = ['stuff', 'badger_number_one.txt']
    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile(filesToWrite[0], 'tree', (err) => {
        assert.error(err, 'no error')
        drive.writeFile(filesToWrite[1], 'peanut', (err) => {
          assert.error(err, 'no error')
          drive.readdir('/', (err, files) => {
            assert.error(err, 'no error')
            assert.deepEqual(files.sort(), expectedReaddirOutput.sort(), 'files are the same')
            drive.readdir('/stuff', (err, files) => {
              assert.error(err, 'no error')
              assert.equal('things', files[0], 'can specify directory')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })
})

describe('multiwriter', (context) => {
  context('replicate to empty drive', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var drive = KappaDrive(storage1)
    var drive2 = KappaDrive(storage2)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        sync()
      })
    })

    function sync () {
      replicate(drive, drive2, (err) => {
        assert.error(err, 'no error')
        drive2.ready(() => {
          assert.error(err, 'no error')
          drive2.readFile('/hello.txt', 'utf-8', (err, data) => {
            assert.error(err, 'no error')
            assert.same(data, 'world', 'gets latest value')
            cleanup([storage1, storage2], next)
          })
        })
      })
    }
  })

  context('defaults to latest value', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var drive = KappaDrive(storage1)
    var drive2 = KappaDrive(storage2)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', 'mundo', (err) => {
          assert.error(err, 'no error')
          sync()
        })
      })
    })

    function writeSecond (cb) {
      drive2.writeFile('/hello.txt', 'verden', (err) => {
        assert.error(err, 'no error')
        cb()
      })
    }

    function sync () {
      drive2.ready(() => {
        drive2.writeFile('test.txt', 'testing', (err) => {
          assert.error(err, 'no error')
          replicate(drive, drive2, (err) => {
            assert.error(err, 'no error')
            drive2.readFile('/hello.txt', (err, data) => {
              assert.same(data, Buffer.from('mundo'), 'gets latest value')
              writeSecond(() => {
                replicate(drive, drive2, (err) => {
                  assert.error(err, 'no error')
                  drive.readFile('/hello.txt', (err, data) => {
                    assert.error(err, 'no error')
                    assert.same(data, Buffer.from('verden'), 'gets latest value')
                    cleanup([storage1, storage2], next)
                  })
                })
              })
            })
          })
        })
      })
    }
  })

  context('defalts to latest value with file descriptors', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var drive = KappaDrive(storage1)
    var drive2 = KappaDrive(storage2)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive2.ready(() => {
        drive.open('/hello.txt', 'w', (err, fd) => {
          var data = Buffer.from('world')
          drive.write(fd, data, 0, 5, null, (err, bytesWritten) => {
            assert.error(err, 'no error')
            assert.same(bytesWritten, 5, 'correct number of bytes written')
            drive.close(fd, function (err) {
              assert.error(err, 'no error')
              // TODO this is a hack!  initialise the other drive with a file
              drive2.writeFile('/someOtherFile.txt', 'boop', (err) => {
                assert.error(err, 'no error')
                sync()
              })
            })
          })
        })
      })
    })

    function sync () {
      replicate(drive, drive2, (err) => {
        assert.error(err, 'no error')
        drive2.open('/hello.txt', 'r', (err, fd) => {
          assert.error(err, 'no error')
          assert.ok(fd, 'valid file descriptor')
          var data = Buffer.alloc('world'.length)
          drive2.read(fd, data, 0, 5, null, (err, bytesRead, readData) => {
            assert.error(err, 'no error')
            assert.same(readData, Buffer.from('world'), 'gets latest value')
            cleanup([storage1, storage2], next)
          })
        })
      })
    }
  })

  context('defaults to latest value with streams', (assert, next) => {
    var drive = KappaDrive(ram)
    var drive2 = KappaDrive(ram)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive2.ready(() => {
        var ws = drive.createWriteStream('/hello.txt')
        ws.on('finish', sync)
        ws.end('mundo')
      })
    })

    function writeSecond (cb) {
      var ws = drive2.createWriteStream('/hello.txt')
      ws.on('finish', cb)
      ws.on('error', assert.error)
      ws.end('verden')
    }

    function sync () {
      replicate(drive, drive2, (err) => {
        assert.error(err, 'no error')
        writeSecond(() => {
          replicate(drive, drive2, (err) => {
            assert.error(err, 'no error')
            var rs = drive.createReadStream('/hello.txt')
            rs.on('data', (data) => {
              assert.same(data, Buffer.from('verden'), 'gets latest value')
              next()
            })
          })
        })
      })
    }
  })
})

// TODO: figure out why this resolves incorrectly when in memory
describe('conflict', (context) => {
  context('fork', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var drive = KappaDrive(storage1)
    var drive2 = KappaDrive(storage2)

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive2.ready(() => {
        var ws = drive.createWriteStream('/hello.txt')
        ws.end('world')
        ws.on('finish', (err) => replicate(drive, drive2, writeFork))
      })
    })

    function writeFork () {
      var pending = 2
      var done = () => !--pending && replicate(drive, drive2, checkFork)
      var ws = drive.createWriteStream('/hello.txt')
      ws.on('finish', done)
      ws.end('mundo')
      var ws2 = drive2.createWriteStream('/hello.txt')
      ws2.on('finish', done)
      ws2.end('verden')
    }

    function checkFork () {
      var ws = drive.createWriteStream('/hello.txt')
      ws.end('whateverr')
      ws.on('finish', () => {
        drive2.readFile('/hello.txt', 'utf-8', (err, data) => {
          assert.error(err, 'no error')
          assert.same(data, 'verden')
          drive.readFile('/hello.txt', 'utf-8', (err, data) => {
            assert.error(err, 'no error')
            assert.same(data, 'whateverr', 'forked values')
            cleanup([storage1, storage2], next)
          })
        })
      })
    }
  })
})

describe('read access', (context) => {
  context('accepts a top-level key for replication', (assert, next) => {
    var accessKey = crypto.randomBytes(32)

    var drive = KappaDrive(ram, { encryptionKey: accessKey })

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      assert.same(drive.key, accessKey, 'drive key is access key')
      assert.same(drive._feeds._root.key, accessKey, '_root key is access key')
      var keys = [
        { name: 'metadata', key: drive.metadata.key },
        { name: 'content', key: drive.content.key }
      ]
      keys.forEach((obj) => {
        assert.notEqual(obj.key, accessKey, `${obj.name} key is different to the access key`)
      })
      next()
    })
  })
})

describe('replicate with derived keypair', (context) => {
  context('regular hypercore using derived keypair', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var master = masterKey()
    var keys = keyPair(master, 0)

    assert.ok(keys, 'generates keys')

    var core1 = hypercore(storage1, keys.publicKey, { secretKey: keys.secretKey })
    var core2 = hypercore(storage2, keys.publicKey)

    var dog = 'dog'
    core1.append(dog, (err, seq) => {
      assert.error(err, 'no error')
      replicate(core1, core2, (err) => {
        assert.error(err, 'no error')

        core2.get(0, (err, msg) => {
          assert.error(err, 'no error')
          assert.same(msg.toString(), dog, 'gets the dog')
          cleanup([storage1, storage2], next)
        })
      })
    })
  })

  context('using a master key, defaults to latest value', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()

    var master1 = masterKey()
    var master2 = masterKey()

    var keyPair1 = keyPair.bind(null, master1)
    var keyPair2 = keyPair.bind(null, master2)

    var drive = KappaDrive(storage1, { keyPair: keyPair1 })
    var drive2 = KappaDrive(storage2, { keyPair: keyPair2 })

    drive.ready((err) => {
      assert.error(err, 'No error on drive.ready')
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        drive.writeFile('/hello.txt', 'mundo', (err) => {
          assert.error(err, 'no error')
          sync()
        })
      })
    })

    function writeSecond (cb) {
      drive2.writeFile('/hello.txt', 'verden', (err) => {
        assert.error(err, 'no error')
        cb()
      })
    }

    function sync () {
      drive2.ready(() => {
        drive2.writeFile('test.txt', 'testing', (err) => {
          assert.error(err, 'no error')
          replicate(drive, drive2, (err) => {
            assert.error(err, 'no error')
            writeSecond(() => {
              replicate(drive, drive2, (err) => {
                assert.error(err, 'no error')
                drive.readFile('/hello.txt', (err, data) => {
                  assert.error(err, 'no error')
                  assert.same(data, Buffer.from('verden'), 'gets latest value')
                  cleanup([storage1, storage2], next)
                })
              })
            })
          })
        })
      })
    }
  })
})

function masterKey () {
  const key = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
  sodium.crypto_kdf_keygen(key)
  return key
}

function keyPair (masterKey, id, ctxt = 'kappadrive') {
  const context = sodium.sodium_malloc(sodium.crypto_hash_sha256_BYTES)
  sodium.crypto_hash_sha256(context, Buffer.from(ctxt))
  const seed = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
  sodium.crypto_kdf_derive_from_key(seed, id, context, masterKey)
  return crypto.keyPair(seed)
}
