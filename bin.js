#!/usr/bin/env node
const minimist = require('minimist')
const chalk = require('chalk')
const RAM = require('random-access-memory')
const hyperswarm = require('hyperswarm')
const pump = require('pump')
const path = require('path')
const level = require('level')
const memdb = require('memdb')
const mkdirp = require('mkdirp')
const mount = require('kappa-drive-mount')
const sodium = require('sodium-universal')
const encoder = require('crypto-encoder')
const crypto = require('hypercore-crypto')
const { isFunction } = require('util')

const KappaDrive = require('./')

Application(minimist(process.argv.slice(2)))

function Application (opts) {
  let valueEncoding

  if (opts.help || opts.h) {
    console.log(usage())
    process.exit(0)
  }

  if (opts.new || opts.n) {
    opts.key = hex(crypto.randomBytes(32))
  }

  if (opts.encryptionKey || opts.e) {
    valueEncoding = encoder(Buffer.from(opts.encryptionKey, 'hex'))
  }

  if (!opts.mount) opts.mount = opts.m || './mnt'

  if (!opts.key) {
    console.log(chalk.red('error:'), 'you must provide an argument:', chalk.blue('--key'), 'or', chalk.blue('--new'))
    console.log()
    console.log(usage())
    process.exit(1)
  }

  console.log(`\n `, chalk.green.underline(`Kappa-Drive`), `(also known as peerfs)`)

  var storage = defaultStorage(opts.db)
  var views = defaultViews(storage)

  var drive = KappaDrive(storage, opts.key, {
    db: views,
    valueEncoding: valueEncoding || 'utf8'
  })

  drive.ready(() => {
    var swarm = Swarm(drive)
    swarm.join(drive.discoveryKey, { lookup: true, announce: true })

    console.log(`
  Drive Key: ${chalk.blue(hex(drive.key))} 
  Swarm Address: ${chalk.blue(hex(drive.discoveryKey))}${opts.encryptionKey ? `\n  Encryption Key: ${chalk.blue(opts.encryptionKey)}` : ''}`)

    mount(drive, opts.mount, (err, unmount) => {
      console.log(`
  Storage: ${ isFunction(storage) ? chalk.green('in memory') : `${chalk.green(`file://${path.resolve(storage)}`)}`}
  Mount: ${chalk.green(`file://${path.resolve(opts.mount)}`)}`)

      process.once('SIGINT', () => {
        console.log(chalk.red('\n  Dropping peer connections and exiting the swarm...'))
        swarm.leave(drive.discoveryKey)

        swarm.destroy(() => {
          console.log(chalk.red('  Terminating FUSE mount...'))
          unmount(() => {
            console.log(chalk.red('  Closing the drive...'))
            drive.close()
            console.log(`\n  ${chalk.green('Success!')}\n\n  Thanks for using Kappa-Drive.`)
            process.exit(0)
          })
        })
      })
    })
  })

  function usage () {
    return `Usage

  Create a new kappa-drive
    kappa-drive --new

  Join a kappa-drive by its key
    kappa-drive --key {key}

  Options:
    --new                 Start a new kappa-drive
    --key                 Specify a specific key / address / topic. default: ${chalk.green('bee80ff3a4ee5e727dc44197cb9d25bf8f19d50b0f3ad2984cfe5b7d14e75de7$')}
    --encryption-key      Use a given encryption key. default: ${chalk.green('none')}
    --mnt                 Choose a mount directory for your kappa-drive. default: ${chalk.green('"./mnt"')}
    --db                  Specify a storage path for kappa-drive's hyperdrives. default: ${chalk.green('RAM')}
    --help                Print this help message

Learn more at ${chalk.blue(`https://gitlab.com/coboxcoop/kappa-drive`)}`
  }
}

function Swarm (drive, opts = {}) {
  var swarm = hyperswarm()

  swarm.on('connection', (socket, details) => {
    if (details.peer) {
      console.log(`
  ${chalk.green('Connection')}
  Peer ID: ${chalk.blue(hex(genericHash(details.peer.host)))}`)
    }

    pump(socket, drive.replicate(details.client), socket)
  })

  swarm.on('disconnection', (socket, details) => {
    if (details.peer) {
      console.log(`
  ${chalk.red('Disconnection')}
  Peer ID: ${chalk.blue(hex(genericHash(details.peer.host)))}`)
    }
  })

  return swarm
}

function defaultStorage (storage) {
  if (storage) return path.resolve(storage)
  return RAM
}

function defaultViews (storage) {
  if (isFunction(storage)) return memdb()
  var views = path.join(path.resolve(storage), 'views')
  mkdirp.sync(views)
  return level(views)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

function genericHash (input) {
  var hash = Buffer.alloc(sodium.crypto_generichash_BYTES_MIN)
  sodium.crypto_generichash(hash, Buffer.from(input))
  return hash
}
