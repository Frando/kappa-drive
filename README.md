# kappa-drive

A multiwriter peer-to-peer filesystem built on [`kappa-core`](https://github.com/kappa-db/kappa-core).

Supports [Hyperdrive V10](https://github.com/mafintosh/hyperdrive/). Originally known as 'peerfs'.

Designed to be compatible with [`hyperdrive-fuse`](https://github.com/andrewosh/hyperdrive-fuse/)

## Installation

To use the CLI...

```
npm install -g kappa-drive

kappa-drive --help
```

or as an npm package

```
# with optional dependencies includes FUSE and hyperswarm for the CLI script to work
npm install

# or without the fluff, just a raw kappa-drive
npm install --no-optional
```

## Usage

```js
const KappaDrive = require('.')
var aliceDrive = KappaDrive('./alice-drive')
var bobDrive = KappaDrive('./bob-drive')

aliceDrive.ready(() => {
  aliceDrive.writeFile('/hello.txt', 'world', (err) => {
    if (err) throw err
    bobDrive.ready(() => {
      replicate(bobOverwrite)
    })
  })
})

function bobOverwrite () {
  bobDrive.writeFile('/hello.txt', 'mundo', (err) => {
    if (err) throw err
    check()
  })
}

function replicate (cb) {
  var aliceStream = aliceDrive.replicate(true, { live: false })
  aliceStream.pipe(bobDrive.replicate(false, { live: false })).pipe(aliceStream)
  aliceStream.on('end', cb)
}

function check () {
  replicate(() => {
    aliceDrive.readFile('/hello.txt', 'utf-8', (err, data) => {
      if (err) throw err
      console.log(data) // mundo
    })
  })
}
```

See [test/index.test.js](test/index.test.js) for more examples.

## API

#### ```var drive = KappaDrive(storage, key, options)```

Returns a kappa-drive instance instance. `options` are passed directly to an instance of [KappaCore](kappa-db/kappa-core).

In addition, peer-fs accepts the following optional arguments:

* `resolveFork`: a function taking `values` of known versions of that file. If there are more than one here, there is a fork. By default, this returns the first item in the list. Overwrite this to get more fancy. 

#### ```drive.key```

The public key identifying the drive

#### ```drive.discoveryKey```

The discovery key identifying the drive

#### ```drive.ready(callback)```

Callback is called when the drive is ready and properties are populated.

#### ```drive.replicate(isInitiator, options)```

* `isInitiator` - should be true if you are initiating the replication.
Replicate between two `peer-fs` instances. Behaves similar to `kappacore.replicate`

#### ```drive.readFile(filename, options, callback)```

Read a file asyncronously. Behaves similar to `hyperdrive.readFile` or `fs.readFile`

#### ```drive.writeFile(filename, content, callback)```

Write a file asyncronously. Behaves similar to `hyperdrive.writeFile` or `fs.writeFile`

#### ```drive.createWriteStream(filename, callback)```

Write a stream to a file. Behaves similar to `hyperdrive.createWriteStream` or `fs.createWriteStream`

#### ```drive.createReadStream(filename, [options])```

Read file from a stream. Behaves similar to `hyperdrive.createReadStream` or `fs.creadReadStream`

#### ```drive.exists(filename, callback)```

Check a file exists.

#### ```drive.truncate(filename, size, callback)```

Truncate a file to `size`. Behaves similar to `hyperdrive.truncate` or 

#### ```drive.readdir(name, [options], callback)```

List all files within a specified directory path. Behaves similar to `hyperdrive.readdir` or `fs.readdir`

#### ```drive.stat(filename, opts, callback)```

Callback with an object containing information about a file. Behaves similar to `hyperdrive.stat` or `fs.stat`

#### ```drive.lsstat(filename, opts, callback)```

Callback with an object containing information about a file (without following symbolic links). Behaves similar to `hyperdrive.lstat` or `fs.lstat`

#### ```drive.unlink(target, callback)```

Delete a file.  Behaves similar to `hyperdrive.unlink` or `fs.unlink` 

#### ```drive.mkdir(directory, callback)```

Create a directory in the archive. Behaves similar to `hyperdrive.mkdir` or `fs.mkdir`

#### ```drive.rmdir(directory, callback)```

Remove a directory from the archive. Behaves similar to `hyperdrive.rmdir` or `fs.mkdir`

#### ```drive.open(filename, flags, callback)```

Open a file and get a file descriptor in the callback. Behaves similar to `hyperdrive.open` or `fs.open`.  Currently only limited support for random access writes.

#### ```drive.close(fd, callback)```

Close a file referred to by a given file descriptor. Behaves similar to `hyperdrive.close` or `fs.close`.  Note that any writes made to the file will only be applied when the file is closed.

#### ```drive.read(fd, buffer, offset, length, position, callback)```

Read from a file descriptor into a buffer. Behaves similar to `hyperdrive.read` or `fs.read`.

#### ```drive.write(fd, buffer, offset, length, position, callback)```

Write from a buffer to a file descriptor.  Positional writes are not currently supported, only overwrite or append. Behaves similar to `hyperdrive.write` or `fs.write`


#### ```drive.symlink(target, linkname, callback)``` 

Create a symbolic link from `linkname` to `target`. Behaves similar to `hyperdrive.symlink` or `fs.symlink`.

#### ```drive.create(filename, [mode], callback)```

Create a file (used when opening a new file in write mode with fuse). Behaves similar to `hyperdrive.create`

## Bugs / issues

Please add issues here: https://ledger-git.dyne.org/CoBox/kappa-drive/issues

## License

AGPL-3.0.0-or-later

## Credit & Thanks

Huge credit to [Karissa](https://github.com/karissa) for ideating and writing [peerfs](https://github.com/karissa/peerfs) and allowing us to continue the project and complete the API.

Thanks to [Frando](https://github.com/frando) for helping with the recent upgrade to [kappa-core](https://github.com/frando/kappa-core) and [kappa-sparse-indexer](https://github.com/frando/kappa-sparse-indexer).

:purple_heart: :green_heart:
